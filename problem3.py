'''Program to find extra run conceded by per team in 2016'''
import csv
import matplotlib.pyplot as plt
def plot(plot_object):
    '''Display the graph'''
    team_name = list()
    per_team_extra_runs = list()
    for team_names, extra_run in plot_object.items():
        team_name.append(team_names)
        per_team_extra_runs.append(extra_run)
    plt.bar(team_name, per_team_extra_runs, width=0.7, color='c')
    plt.xlabel('Team name    -------------------->')
    plt.ylabel("Number's of extra runs    -------------------->")
    plt.title("Extra run conceded by per team in 2016 ")
    plt.xticks(rotation=10, fontsize='x-small')
    plt.show()

def extra_run_conceded_per_team_in_2016(csv_location):
    '''Return a dictionary which containing mathes won by teams in evry season'''
    run_conced_per_team = dict()
    with open(csv_location) as deliveris_object:
        deliveris_reader = csv.DictReader(deliveris_object)
        match_id = list(range(577, 637))
        for delivery in deliveris_reader:
            if int(delivery['match_id']) in match_id and delivery['is_super_over'] == '0':
                if delivery['bowling_team'] in run_conced_per_team:
                    run_conced_per_team[delivery['bowling_team']] = \
                        int(run_conced_per_team[delivery['bowling_team']]) + int(delivery['extra_runs'])
                else:
                    run_conced_per_team[delivery['bowling_team']] = int(delivery['extra_runs'])
    return run_conced_per_team

def execute():
    '''Execute problem3'''
    plot(extra_run_conceded_per_team_in_2016("ipl/deliveries.csv"))
execute()

'''program to find Top 10 Bowler's in 2015'''
import csv
import operator
import matplotlib.pyplot as plt


def plot(plot_object):
    '''Display the graph'''
    player_name = list()
    player_economy = list()
    for only_ten in range(0, 10):
        player_name.append(plot_object[only_ten][0])
        player_economy.append(plot_object[only_ten][1])
    plt.bar(player_name, player_economy, width=0.7, color='c')
    plt.xlabel("Player's name   -------------------->")
    plt.ylabel("Economy    -------------------->")
    plt.title("Top 10 Bowler's in 2015")
    plt.show()

def top_economical_bowlers_in_2015(csv_location):
    '''Return a dictionry which containing to 10 bowlers in 2015'''
    bowler_stats = dict()
    bowler_economy_rates = dict()
    match_id = list(range(518, 577))
    with open(csv_location) as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        for delivery in deliveries_reader:
            if int(delivery['match_id']) in match_id and delivery['is_super_over'] == '0':
                if delivery['bowler'] in bowler_stats:
                    if int(delivery['ball']) <= 6:
                        bowler_stats[delivery['bowler']][0] = \
                            bowler_stats[delivery['bowler']][0] + 1
                        bowler_stats[delivery['bowler']][1] = \
                            bowler_stats[delivery['bowler']][1] + int(delivery['total_runs'])
                    else:
                        bowler_stats[delivery['bowler']][1] = \
                            bowler_stats[delivery['bowler']][1] + int(delivery['total_runs'])
                else:
                    bowler_stats[delivery['bowler']] = list()
                    bowler_stats[delivery['bowler']].append(1)
                    bowler_stats[delivery['bowler']].append(int(delivery['total_runs']))
    for key, value in bowler_stats.items():
        bowler_economy_rates[key] = (value[1]*6)/value[0]
    return_to_graph_economical_bowlers = sorted(bowler_economy_rates.items(), \
        key=operator.itemgetter(1))
    return return_to_graph_economical_bowlers
def execute():
    '''Execute problem4'''
    plot(top_economical_bowlers_in_2015("ipl/deliveries.csv"))
execute()

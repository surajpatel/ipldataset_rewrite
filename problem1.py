'''Program to find Numbers of match played per year'''
import csv
import matplotlib.pyplot as plt

def plot(plot_object):
    '''Display the graph'''
    years_in_sorted_order = sorted(plot_object)
    no_of_games_in_sorted_order = list()
    for year in years_in_sorted_order:
        no_of_games_in_sorted_order.append(plot_object[year])
    plt.bar(years_in_sorted_order, no_of_games_in_sorted_order, color='c')
    plt.xlabel('Year')
    plt.ylabel("Number's of matches")
    plt.title("Number\'s of match played per year")
    plt.show()

def no_of_match_played_per_year(csv_location):
    '''return a dictionary which containing no_of_match_per_year'''
    data = dict()
    with open(csv_location) as matches_csv:
        match_reader = csv.DictReader(matches_csv)
        for match in match_reader:
            if match['season'] in data:
                data[match['season']] = data[match['season']] + 1
            else:
                data[match['season']] = 1
    return data

def execute():
    '''Execute problem1'''
    plot(no_of_match_played_per_year("ipl/matches.csv"))
execute()

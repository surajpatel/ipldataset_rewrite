'''program to find Top 10 players, Player of the match of all season'''
import csv
import operator
import matplotlib.pyplot as plt

def plot(plot_object):
    '''Display the graph'''
    player_name = list()
    no_of_times = list()
    for value_in_tuple in plot_object[:10]:
        player_name.append(value_in_tuple[0])
        no_of_times.append(value_in_tuple[1])
    plt.bar(player_name, no_of_times, color='c')
    plt.xlabel("Player's name   -------------------->")
    plt.ylabel("Player of the match    -------------------->")
    plt.title("Top 10 players, Player of the match of all season")
    plt.show()

def top_10_players_of_the_match(csv_location):
    '''Return a dictionary which containing top 10 players of the match of all season'''
    players_of_match = dict()
    with open(csv_location) as match_object:
        match_reader = csv.DictReader(match_object)
        for match in match_reader:
            if match['player_of_match'] in players_of_match:
                players_of_match[match['player_of_match']] = \
                    players_of_match[match['player_of_match']] + 1
            else:
                players_of_match[match['player_of_match']] = 1
        players_of_match_in_order = sorted(players_of_match.items(), \
            key=operator.itemgetter(1), reverse=True)
        return players_of_match_in_order
def execute():
    '''Execute problem 6'''
    plot(top_10_players_of_the_match("ipl/matches.csv"))
execute()

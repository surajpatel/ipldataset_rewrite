'''program to find Right Decision taken by teams'''
import csv
import matplotlib.pyplot as plt


def plot(plot_object):
    '''Display the graph'''
    teams_in_order = sorted(plot_object)
    no_of_games_in_sorted_order = list()
    for year in teams_in_order:
        no_of_games_in_sorted_order.append(plot_object[year])
    plt.bar(teams_in_order, no_of_games_in_sorted_order, color='c')
    plt.xlabel('Team name')
    plt.ylabel("Number's of matches")
    plt.title("Right Decision taken by teams")
    plt.xticks(rotation=10, fontsize='x-small')
    plt.show()

def right_decision(*team_name_and_year):
    '''return a dictionary which containing no of teams and no of right decision'''
    save_year_and_team_name = dict()
    with open(team_name_and_year[0]) as matches_csv:
        match_reader = csv.DictReader(matches_csv)
        for line in match_reader:
            if line['toss_winner'] == line['winner']:
                if line['season'] in save_year_and_team_name:
                    save_year_and_team_name[line['season']] = \
                        save_year_and_team_name[line['season']] + '-' + line['winner']
                else:
                    save_year_and_team_name[line['season']] = line['winner']
    year_wise_teams = dict()
    for team_name in save_year_and_team_name:
        year_wise_teams[team_name] = dict()
        count_no_of_teams = dict()
        list_of_teams = save_year_and_team_name[team_name].split('-')
        for team in list_of_teams:
            if team in count_no_of_teams:
                count_no_of_teams[team] = count_no_of_teams[team] +1
            else:
                count_no_of_teams[team] = 1
        year_wise_teams[team_name] = count_no_of_teams

    if len(team_name_and_year) < 2:
        team_win_dict = dict()
        for year in year_wise_teams:
            for team in year_wise_teams[year]:
                if team in team_win_dict:
                    team_win_dict[team] = team_win_dict[team] + year_wise_teams[year][team]
                else:
                    team_win_dict[team] = year_wise_teams[year][team]
        plot(team_win_dict)
    else:
        if str(team_name_and_year[1]) in year_wise_teams:
            plot(year_wise_teams[str(team_name_and_year[1])])
        else:
            print('invalid Season')

def execute():
    '''Execute problem 7'''
    right_decision("ipl/matches.csv", 2016)
execute()

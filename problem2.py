'''program to find Match won by team's in every season'''
import csv
import matplotlib.pyplot as plt

def plot(plot_object):
    '''Display the graph'''
    teams_name = list()
    per_year_team_win = list()
    result_in_dict_values = list()
    for key, value in plot_object.items():
        teams_name.append(key)
        per_year_team_win.append(value)
    for order_in in per_year_team_win:
        result_in_dict_values.append(dict(sorted(order_in.items())))
    list_conatin_every_year_win_data = list()
    years = list()
    count = 0
    for year in range(2008, 2018):
        years.append(year)

    while count < 14:
        temp_win_number_hold = list()
        for keys, values in result_in_dict_values[count].items():
            temp_win_number_hold.append(values)
        count += 1
        list_conatin_every_year_win_data.append(temp_win_number_hold)
    bottom_point = [0]*10
    for win_count in list_conatin_every_year_win_data:
        team_lable = teams_name[list_conatin_every_year_win_data.index(win_count)]
        plt.bar(years, win_count, width=0.5, bottom=bottom_point, label=team_lable)
        bottom_point = [x+y for (x, y) in zip(win_count, bottom_point)]
    plt.ylabel('No of Matches    ----------------->')
    plt.xlabel("Years    ----------------->")
    plt.title("Match won by team's in every season", y=-0.15)
    plt.legend(bbox_to_anchor=(1.05, 1), ncol=5, loc=4, borderaxespad=1)
    plt.show()

def matches_won_of_all_teams_over_all_the_years(csv_location):
    '''Return a dictionary which contain match won by teams in every season'''
    data = dict()
    with open(csv_location) as matches_csv:
        match_reader = csv.DictReader(matches_csv)
        for match in match_reader:
            if match['winner'] in data:
                if match['winner'] != "":
                    data[match['winner']] = data[match['winner']] +"-" +match['season']
            else:
                if match['winner'] != "":
                    data[match['winner']] = match['season']
    team_year_count_wise_data = dict()
    for team_name in data:
        one_team_win_in_years = data[team_name].split("-")
        team_year_count_wise_data[team_name] = dict()
        for year in one_team_win_in_years:
            if year is not team_year_count_wise_data[team_name]:
                team_year_count_wise_data[team_name][int(year)] \
                    = int(one_team_win_in_years.count(year))
    for team_name in team_year_count_wise_data:
        for year in range(2008, 2018):
            if year in team_year_count_wise_data[team_name]:
                pass
            else:
                team_year_count_wise_data[team_name][year] = 0
    return team_year_count_wise_data

def execute():
    '''Execute problem'''
    plot(matches_won_of_all_teams_over_all_the_years("ipl/matches.csv"))
execute()

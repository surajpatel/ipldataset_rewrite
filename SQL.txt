1-> select distinct(season) as Year,count(id) as No_of_match from matches group by season;


2->select winner, season, count(winner) from matches where winner is not null group by winner,season order by winner;


3->select bowling_team, sum(extra_runs) as Extra_runs from deliveries d,matches m where d.match_id = m.id and m.season::integer in (2016) group by bowling_team;

4->select bowler,(sum(batsman_runs+noball_runs+wide_runs)*6.00/count(bowler)) as economy from deliveries d,matches m where d.match_id=m.id and m.season::integer in (2015) group by bowler order by economy asc limit 10;
